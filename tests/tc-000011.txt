>>>test.tc-000011

title:: Library.pm Unit Test Case

priority:: routine

type:: unit

is_part_of:: tc-000001

description:: This is the unit test case for the Library.pm Perl
module that implements the SML::Library class.

index:: tc-000011; unit test case!Library.pm

>>>LISTING.lis-library-pm-unit-test-results

title:: Library.pm Unit Test Case Results

numbers:: left

fontsize:: footnotesize

script:: scripts/run-unit-test.bat Library.t

<<<LISTING

>>>LISTING.lis-library-pm-unit-test-case

title:: Library.t - Unit Test Case for Library.pm

numbers:: left

language:: Perl

orientation:: portrait

fontsize:: footnotesize

file:: ../semantic-manuscript-language/t/Library.t

<<<LISTING

<<<test
