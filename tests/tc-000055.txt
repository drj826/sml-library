>>>test.tc-000055

title:: Entity.pm Unit Test Case

priority:: routine

type:: unit

is_part_of:: tc-000001

description:: This is the unit test case for the Entity.pm Perl
module that implements the SML::Entity class.

index:: tc-000055; unit test case!Entity.pm

>>>LISTING.lis-item-pm-unit-test-results

title:: Entity.pm Unit Test Case Results

numbers:: left

fontsize:: footnotesize

script:: scripts/run-unit-test.bat Entity.t

<<<LISTING

>>>LISTING.lis-item-pm-unit-test-case

title:: Entity.t - Unit Test Case for Entity.pm

numbers:: left

language:: Perl

orientation:: landscape

fontsize:: footnotesize

file:: ../semantic-manuscript-language/t/Entity.t

<<<LISTING

<<<test
