>>>configuration_item.ci-000424

title:: SML::Listitem Class

type:: software

is_part_of:: ci-000510          # SML Perl Modules

specializes:: ci-000387         # SML::Block Class

description:: An SML::Listitem represents an item in some type of list
like a bullet list, enumerated list, or definition list.

index:: ci-000424; Listitem.pm; SML::Listitem; list item

>>>FIGURE.fig-class-diagram-list-item

title:: SML::ListItem Class Diagram

image:: files/images/class-diagram-list-item.png

<<<FIGURE

>>>LISTING.lis-listitem-pod

title:: SML::ListItem Documentation

plugin:: Pod2Txt ../semantic-manuscript-language/lib/SML/ListItem.pm

<<<LISTING

>>>LISTING.lis-listitem-pm

title:: ListItem.pm Code Listing

numbers:: left

language:: Perl

fontsize:: footnotesize

file:: ../semantic-manuscript-language/lib/SML/ListItem.pm

<<<LISTING

<<<configuration_item
